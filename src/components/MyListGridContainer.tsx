import * as React from 'react';
import { FocusZone } from 'office-ui-fabric-react/lib/FocusZone';
import { List } from 'office-ui-fabric-react/lib/List';
import { ITheme, getTheme, mergeStyleSets } from 'office-ui-fabric-react/lib/Styling';
import { DocumentCardCompleteExample } from "./DocumentCardCompleteExample";
import {FonoViewModel} from '../viewmodel/FonoViewModel';
import DocumentCardFonoDevice from './DocumentCardFonoDevice';
import { observer } from 'mobx-react';

interface IListGridContainerProps {
    viewModel: FonoViewModel;
}

//Styles
const classNames = mergeStyleSets({
  container: {
    fontSize: 0,
    position: 'relative',
    marginTop: 10,
    marginBottom: 10,
    overflow: 'auto',
    maxHeight: '73vh',
    paddingRight: 5,
    border: '1px solid grey',
  }
});
@observer
class MyListGridContainer extends React.Component<IListGridContainerProps> {
  
  public render(): JSX.Element {
    return (
      <div data-is-scrollable="true" style={{overflow: "auto"}}>
      <FocusZone>
        <List
          className={classNames.container}
          items={this.props.viewModel.rootModel.DevicesModel.devices}
          onRenderCell={this._onRenderCell}
        />
      </FocusZone>
      </div>
    );
  }

  private _onRenderCell = (item: any, index: number | undefined): JSX.Element => {
    return (
        <DocumentCardFonoDevice device={item}/>
    );
  };
}

export {MyListGridContainer as default, MyListGridContainer, IListGridContainerProps};