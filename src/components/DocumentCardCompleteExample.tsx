import * as React from 'react';
import {
  DocumentCard,
  DocumentCardActions,
  DocumentCardActivity,
  DocumentCardLocation,
  DocumentCardPreview,
  DocumentCardTitle,
  IDocumentCardPreviewProps,
  IDocumentCardStyles,
  DocumentCardType,
  DocumentCardImage,
  DocumentCardDetails,
  DocumentCardStatus,
  IDocumentCardDetailsStyles,
  IDocumentCardStatusStyles
} from 'office-ui-fabric-react/lib/DocumentCard';
import { ImageFit } from 'office-ui-fabric-react/lib/Image';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { TestImages } from '@uifabric/example-data';
import { Stack, IStackStyles, IStackTokens, IStackItemStyles, FontSizes,} from 'office-ui-fabric-react';

//Test data
const exampleData: any = {
    name: "Colin Alenby Digby Forsyth Montgomery Fortescue III",
    det1: "Fred",
    det2: "Norwich",
    det3: "Narrow Boat",
    det4: "Sausage",
    det5: "Shepherd",
    det6: "1908768",
    det7: "0448897652",
    det8: "True",
    det9: "Bonkers",
    det10: "Tuscany Bob",
    det11: "Delilah",
    det12: "Blah",
};


//Styles
const _documentCardStyles: IDocumentCardStyles = {
  root: {
      margin: 10,
      padding:10,
      maxWidth: 3000,
      border: '1px solid lightgrey',
      width: 720,
      float: 'left'
  }
};

const docCardDetailsStyles: IDocumentCardDetailsStyles = {
    root: {
        marginLeft: 16,
        marginBottom: 10
    }
};

const docCardTitleStyles: IDocumentCardDetailsStyles = {
    root: {
        marginTop: 10,
        height: 32,
        fontSize: FontSizes.xxLarge
    }
};

const docCardStatusStyles: IDocumentCardStatusStyles = {
    root: {
        paddingLeft: 10,
        paddingTop: 10
    }
};

const stackTokens: IStackTokens = {
  childrenGap: 10,
  padding: 10
}

class DocumentCardCompleteExample extends React.PureComponent {
  public render(): JSX.Element {
    const previewProps: IDocumentCardPreviewProps = {
      getOverflowDocumentCountText: (overflowCount: number) => `+${overflowCount} more`,
      previewImages: [
        {
          name: '2016 Conference Presentation',
          linkProps: {
            href: 'http://bing.com',
            target: '_blank'
          },
          previewImageSrc: TestImages.documentPreview,
          iconSrc: TestImages.iconPpt,
          imageFit: ImageFit.cover,
          width: 318,
          height: 196
        },
        {
          name: 'New Contoso Collaboration for Conference Presentation Draft',
          linkProps: {
            href: 'http://bing.com',
            target: '_blank'
          },
          previewImageSrc: TestImages.documentPreviewTwo,
          iconSrc: TestImages.iconPpt,
          imageFit: ImageFit.cover,
          width: 318,
          height: 196
        },
        {
          name: 'Spec Sheet for design',
          linkProps: {
            href: 'http://bing.com',
            target: '_blank'
          },
          previewImageSrc: TestImages.documentPreviewThree,
          iconSrc: TestImages.iconPpt,
          imageFit: ImageFit.cover,
          width: 318,
          height: 196
        },
        {
          name: 'Contoso Marketing Presentation',
          linkProps: {
            href: 'http://bing.com',
            target: '_blank'
          },
          previewImageSrc: TestImages.documentPreview,
          iconSrc: TestImages.iconPpt,
          imageFit: ImageFit.cover,
          width: 318,
          height: 196
        },
        {
          name: 'Notes from Ignite conference',
          linkProps: {
            href: 'http://bing.com',
            target: '_blank'
          },
          previewImageSrc: TestImages.documentPreviewTwo,
          iconSrc: TestImages.iconPpt,
          imageFit: ImageFit.cover,
          width: 318,
          height: 196
        },
        {
          name: 'FY17 Cost Projections',
          linkProps: {
            href: 'http://bing.com',
            target: '_blank'
          },
          previewImageSrc: TestImages.documentPreviewThree,
          iconSrc: TestImages.iconPpt,
          imageFit: ImageFit.cover,
          width: 318,
          height: 196
        }
      ]
    };

  
    return (
      <DocumentCard
        aria-label="Document Card with multiple items, commands and views. Marketing documents. 6 files were uploaded.
        Created by Annie Lindqvist in February 23, 2016. 432 views."
        onClick={this._onClick}
        styles={_documentCardStyles}
        type={DocumentCardType.normal}
      >
        <DocumentCardTitle styles={docCardTitleStyles}title="Details" />

        <DocumentCardDetails styles={docCardDetailsStyles}>
            <Label>Name</Label>
            <Text variant="medium">{exampleData.name}</Text>

            <Stack horizontal horizontalAlign="start">
                <Stack.Item align="start" grow={true}>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det1}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det2}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det3}</Text>
                </Stack.Item>
                <Stack.Item align="stretch" grow={true}>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det4}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det5}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det6}</Text>
                </Stack.Item>
                <Stack.Item align="end" grow={true}>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det7}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det8}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det9}</Text>
                </Stack.Item>
            </Stack>
            
            <Stack horizontal horizontalAlign="start" tokens={stackTokens}>
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} iconProps={{ iconName: 'OneNoteLogo', styles: { root: { color: '#813a7c', fontSize: '80px', width: '80px', height: '80px' }}}} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.personaFemale} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.choiceGroupBarSelected} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.choiceGroupPieSelected} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.personaFemale} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.personaMale} />

            </Stack>
        </DocumentCardDetails>
        <DocumentCardPreview {...previewProps} />
        <DocumentCardLocation
          location="Marketing Documents"
          locationHref="http://microsoft.com"
          ariaLabel="Location, Marketing Documents"
        />
        <DocumentCardTitle title="6 files were uploaded" />
        <DocumentCardActivity
          activity="Created Feb 23, 2016"
          people={[{ name: 'Annie Lindqvist', profileImageSrc: TestImages.personaFemale }]}
        />
        <DocumentCardActions
          actions={[
            {
              iconProps: { iconName: 'Share' },
              onClick: this._onActionClick.bind(this, 'share'),
              ariaLabel: 'share action'
            },
            {
              iconProps: { iconName: 'Pin' },
              onClick: this._onActionClick.bind(this, 'pin'),
              ariaLabel: 'pin action'
            },
            {
              iconProps: { iconName: 'Ringer' },
              onClick: this._onActionClick.bind(this, 'notifications'),
              ariaLabel: 'notifications action'
            }
          ]}
          views={432}
        />
        <DocumentCardStatus styles={docCardStatusStyles} status="Information">
            <Text variant="large">Groovy</Text>
        </DocumentCardStatus>
      </DocumentCard>
    );
  } 

  //Event handlers
  private _onActionClick(action: string, ev: React.SyntheticEvent<HTMLElement>): void {
    console.log(`You clicked the ${action} action`);
    ev.stopPropagation();
    ev.preventDefault();
  }

  private _onClick(): void {
    console.log('You clicked the card.');
  }
}

export { DocumentCardCompleteExample as default, DocumentCardCompleteExample };