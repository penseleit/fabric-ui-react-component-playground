import * as React from 'react';
import {
  DocumentCard,
  DocumentCardActivity,
  DocumentCardTitle,
  DocumentCardDetails,
  DocumentCardImage,
  IDocumentCardStyles,
  IDocumentCardActivityPerson,
  IDocumentCardProps,
  IDocumentCard
} from 'office-ui-fabric-react/lib/DocumentCard';
import { ImageFit } from 'office-ui-fabric-react/lib/Image';
import { TestImages } from '@uifabric/example-data';

//Test data
const people: IDocumentCardActivityPerson[] = [
    { name: 'Annie Lindqvist', profileImageSrc: TestImages.personaFemale },
    { name: 'Roko Kolar', profileImageSrc: '', initials: 'RK' },
    { name: 'Aaron Reid', profileImageSrc: TestImages.personaMale },
    { name: 'Christian Bergqvist', profileImageSrc: '', initials: 'CB' }
];

//styles
const cardStyles: IDocumentCardStyles = {
    root: { display: 'inline-block', marginRight: 20, marginBottom: 20, width: 600, float: 'left' }
};

class MyDocumentCard extends React.Component<IDocumentCardProps> implements IDocumentCard {

    constructor(props: IDocumentCardProps){
        super(props);
    }

    focus(): void{
        
    }

    render() {

        return (
        <DocumentCard
                aria-label="Document Card with icon. How to make a good design. Last modified by Christian Bergqvist in January 1, 2019."
                styles={cardStyles}
                onClickHref="http://bing.com"
            >
                <DocumentCardImage
                    height={150}
                    imageFit={ImageFit.cover}
                    iconProps={{
                    iconName: 'OneNoteLogo',
                    styles: { root: { color: '#813a7c', fontSize: '120px', width: '120px', height: '120px' } }
                }}
                />
                <DocumentCardDetails>
                <DocumentCardTitle title="How to make a good design" shouldTruncate />
                </DocumentCardDetails>
                <DocumentCardActivity activity="Modified January 1, 2019" people={[people[3]]} />
            </DocumentCard>
        )
    };
}

export {MyDocumentCard as default, MyDocumentCard};