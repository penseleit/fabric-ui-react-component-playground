import * as React from "react";
import '../css/styles.css';
import { List } from 'office-ui-fabric-react/lib/List';
import { mergeStyleSets, getTheme, normalize } from 'office-ui-fabric-react/lib/Styling';
import { DocumentCardCompleteExample } from "./DocumentCardCompleteExample";

interface IMyListContainerProps{
  name: string;
}

//Test data
const _items = ["One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten"];

//Styles
const theme = getTheme();

const styles = mergeStyleSets({
  container: {
    marginTop: 10,
    marginBottom: 10,
    overflow: 'auto',
    maxHeight: '73vh',
    paddingRight: 5,
    border: '1px solid grey',
    selectors: {
      '.ms-List-cell:nth-child(odd)': {
        background: theme.palette.neutralLighter,
        paddingLeft: 10
      },
      '.ms-List-cell:nth-child(even)': {
        paddingLeft: 10
      }
    }
    
  },
  itemContent: [
    
    theme.fonts.xLarge,
    normalize,
    {
      position: 'relative',
      display: 'block',
      borderLeft: '3px solid ' + theme.palette.themePrimary,
      paddingLeft: 27
    } 
  ]
}); 


class MyListContainer extends React.Component<IMyListContainerProps>{

    private _onRenderCell(item: any, index: number | undefined): JSX.Element {
        return (
            <DocumentCardCompleteExample />
        );
    };

    render(){

        return(
           <div className={styles.container} data-is-scrollable={true}>
                <List 
                items={_items}
                onRenderCell={this._onRenderCell}
                />
         </div>
        )
    }
}

export {MyListContainer as default, MyListContainer, IMyListContainerProps}