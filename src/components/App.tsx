import * as React from "react";
import MyCommandBar from "./MyCommandBar";
import { loadTheme, getTheme, getScrollbarWidth, IStackItemTokens  } from 'office-ui-fabric-react';
import { Separator, ISeparatorStyles } from 'office-ui-fabric-react/lib/Separator';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { Label, ILabelStyles } from 'office-ui-fabric-react/lib/Label';
import { Icon, IIconStyles } from 'office-ui-fabric-react/lib/Icon';
import { DefaultPalette, Stack, IStackStyles, IStackTokens, IStackItemStyles, DefaultButton, PrimaryButton,} from 'office-ui-fabric-react';
import { FocusZone } from 'office-ui-fabric-react/lib/FocusZone';
import { TextField, ITextFieldStyles } from 'office-ui-fabric-react/lib/TextField';
import { ScrollablePane, ScrollbarVisibility, IScrollablePaneStyles } from 'office-ui-fabric-react/lib/ScrollablePane';
import { Sticky, StickyPositionType } from 'office-ui-fabric-react/lib/Sticky';
import { MyListGridContainer } from "./MyListGridContainer";
import {FonoViewModel} from "../viewmodel/FonoViewModel";
import {observer} from 'mobx-react';


//Set app theme
/* loadTheme({
    palette: {
        themePrimary: '#bd20ba',
        themeLighterAlt: '#fcf4fc',
        themeLighter: '#f4d4f4',
        themeLight: '#ebb1ea',
        themeTertiary: '#d76cd5',
        themeSecondary: '#c535c2',
        themeDarkAlt: '#aa1da7',
        themeDark: '#8f188d',
        themeDarker: '#6a1268',
        neutralLighterAlt: '#faf9f8',
        neutralLighter: '#f3f2f1',
        neutralLight: '#edebe9',
        neutralQuaternaryAlt: '#e1dfdd',
        neutralQuaternary: '#d0d0d0',
        neutralTertiaryAlt: '#c8c6c4',
        neutralTertiary: '#595959',
        neutralSecondary: '#373737',
        neutralPrimaryAlt: '#2f2f2f',
        neutralPrimary: '#000000',
        neutralDark: '#151515',
        black: '#0b0b0b',
        white: '#ffffff',
    }
}); */



interface IAppProps {name : string};


// Styles definitions
const stackStyles: IStackStyles = {
    root: {
        marginTop:10
    },
    inner:{

    }
};

const searchButtonStackStyles: IStackStyles = {
    root: {
        marginTop: 10,
        marginLeft:6
    }
};

const searchFormStackItemStyles: IStackItemStyles = {
    root: {
        
        width: 300,
        marginLeft: 10,
        marginTop: 0
    }
};

const listStackItemStyles: IStackItemStyles = {
    root: {
      
        height: '100%',
        position: 'relative'
    }
};

const scrollPaneStyles: IScrollablePaneStyles = {
    root:{
        marginLeft: 10
    },
    stickyAbove: {
        marginLeft: 10
    },
    stickyBelow:{},
    stickyBelowItems:{},
    contentContainer:{
        top: 0,
        left: 0,
        right: 2,
       position: 'static',
       marginLeft: 10
    }

};

const filterTextFieldStyles: ITextFieldStyles = {
    root: {},
    fieldGroup: {},
    prefix: {},
    suffix: {},
    field: {},
    icon: {},
    description: {},
    wrapper: {},
    errorMessage: {},
    subComponentStyles: {label: {}}
};

const filterLabelStyles: ILabelStyles ={
    root: {
        display: 'inline',
       
    }
};

const iconStyles: IIconStyles = {
    root: {
        right: 20,
        fontSize: '18px',
        height: '18px',
        width: '18px'
    }
};

const stackTokens: IStackTokens = { childrenGap: 8,   padding: 0 };
const searchButtonStackTokens : IStackTokens = {childrenGap: 8, padding: 0};
const stackItemTokens : IStackItemTokens = {margin: 0, padding: 0};
const listStackItemTokens : IStackItemTokens = {margin: 0, padding: 0};
const searchFormStackItemTokens: IStackTokens = { childrenGap: 0, padding: 0 };

@observer
class App extends React.Component<IAppProps, {}>{

    constructor(props: IAppProps){
        super(props);
        this._alertClicked = this._alertClicked.bind(this);
        this._clear = this._clear.bind(this);
        this._onModelStringChange = this._onModelStringChange.bind(this);
        this._onDeviceStringChange = this._onDeviceStringChange.bind(this);
        this._search = this._search.bind(this);
    }
 
    private _alertClicked(event: React.MouseEvent<any>): void{
        alert(event.button.toString() + " button clicked.")
    }

    private _clear(): void{
        this._fonoViewModel.clearSearchFields();
    }

    private _search():void {
        this._fonoViewModel.searchForDevices();
    }

    private _onDeviceStringChange(event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string): void{
        this._fonoViewModel.updateDeviceSearchString(newValue);
    }

    private _onModelStringChange(event: React.FormEvent<HTMLInputElement | HTMLTextAreaElement>, newValue?: string): void{
        this._fonoViewModel.updateModelSearchString(newValue);
    }

    private _onFilterChange(event: React.SyntheticEvent): void{

    };

    private _disabled: boolean = false;
    private _checked: boolean = true;
    private _fonoViewModel = new FonoViewModel();

    render() {

        return (
            <div>
                <Text variant="xxLargePlus">{this.props.name}</Text>
                <MyCommandBar />
                <Stack verticalFill={true} horizontal horizontalAlign="center" styles={stackStyles} tokens={stackTokens}>

                    <Stack.Item align="start"  verticalFill={true} styles={searchFormStackItemStyles} tokens={searchFormStackItemTokens}> 
                        <TextField value={this._fonoViewModel.deviceString} onChange={this._onDeviceStringChange} label="Device" required placeholder="Enter a device name" />
                        <TextField value={this._fonoViewModel.modelString} onChange={this._onModelStringChange} label="Model" placeholder="Enter a model" />
                        <Stack horizontal horizontalAlign="end" styles={searchButtonStackStyles} tokens={searchButtonStackTokens}>
                        <Stack.Item align="end" >
                                <DefaultButton text="Clear" onClick={this._clear} allowDisabledFocus disabled={this._disabled} checked={this._checked} />
                            </Stack.Item>
                            <Stack.Item align="end" >
                                <PrimaryButton text="Search" onClick={this._search} allowDisabledFocus disabled={this._disabled} checked={this._checked} />
                            </Stack.Item>
                        </Stack>
                    </Stack.Item> 
                    
                    <Stack.Item align="center" verticalFill={true}  tokens={stackItemTokens}>  
                        <Separator vertical alignContent="center" >
                        </Separator>
                    </Stack.Item> 
                   
                    <Stack.Item align="start" verticalFill={true} grow={3} styles={listStackItemStyles} tokens={listStackItemTokens}>  
                            <ScrollablePane styles={scrollPaneStyles} scrollbarVisibility={ScrollbarVisibility.auto}>
                                <Sticky stickyPosition={StickyPositionType.Header}>
                                    <Stack>
                                        <Text variant='xxLarge'>Search Results (Showing x of {this._fonoViewModel.deviceCount})</Text>
                                    </Stack>
                                    <Stack horizontal horizontalAlign='end' tokens={stackTokens}>    
                                            <Label styles={filterLabelStyles}>Filter</Label>
                                            <TextField styles={filterTextFieldStyles} onChange={this._onFilterChange} />
                                    </Stack>
                                </Sticky>
                                
                                <FocusZone >
                                    <MyListGridContainer viewModel={this._fonoViewModel}/>
                                </FocusZone>
                            </ScrollablePane>
                        
                    </Stack.Item>  
                </Stack>
            </div>
        );
    }
};

export {IAppProps, App};