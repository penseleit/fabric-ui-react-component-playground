import * as React from 'react';
import { CommandBar, ICommandBarItemProps } from 'office-ui-fabric-react/lib/CommandBar';
import { IButtonProps, DefaultButton } from 'office-ui-fabric-react/lib/Button';

class MyCommandBar extends React.Component{

    private overflowProps: IButtonProps = { ariaLabel: 'More commands' };

    private items: ICommandBarItemProps[] = [
        {
          key: 'newItem',
          text: 'New',
          cacheKey: 'myCacheKey', // changing this key will invalidate this item's cache
          iconProps: { iconName: 'Add' },
          subMenuProps: {
            items: [
              {
                key: 'emailMessage',
                text: 'Email message',
                iconProps: { iconName: 'Mail' },
                ['data-automation-id']: 'newEmailButton' // optional
              },
              {
                key: 'calendarEvent',
                text: 'Calendar event',
                iconProps: { iconName: 'Calendar' }
              }
            ]
          }
        },
        {
          key: 'upload',
          text: 'Upload',
          iconProps: { iconName: 'Upload' },
          href: 'https://dev.office.com/fabric'
        },
        {
          key: 'share',
          text: 'Share',
          iconProps: { iconName: 'Share' },
          onClick: () => console.log('Share')
        },
        {
          key: 'download',
          text: 'Download',
          iconProps: { iconName: 'Download' },
          onClick: () => console.log('Download')
        }
      ];
      
      private overflowItems: ICommandBarItemProps[] = [
        { key: 'move', text: 'Move to...', onClick: () => console.log('Move to'), iconProps: { iconName: 'MoveToFolder' } },
        { key: 'copy', text: 'Copy to...', onClick: () => console.log('Copy to'), iconProps: { iconName: 'Copy' } },
        { key: 'rename', text: 'Rename...', onClick: () => console.log('Rename'), iconProps: { iconName: 'Edit' } }
      ];
      
      private farItems: ICommandBarItemProps[] = [
        {
          key: 'tile',
          text: 'Grid view',
          // This needs an ariaLabel since it's icon-only
          ariaLabel: 'Grid view',
          iconOnly: true,
          iconProps: { iconName: 'Tiles' },
          onClick: () => console.log('Tiles')
        },
        {
          key: 'info',
          text: 'Info',
          // This needs an ariaLabel since it's icon-only
          ariaLabel: 'Info',
          iconOnly: true,
          iconProps: { iconName: 'Info' },
          onClick: () => console.log('Info')
        }
      ];

    render(){
        return (
            <div>
              <CommandBar
                items={this.items}
                overflowItems={this.overflowItems}
                overflowButtonProps={this.overflowProps}
                farItems={this.farItems}
                ariaLabel="Use left and right arrow keys to navigate between commands"
              />
            </div>
          );
    };
}




export {MyCommandBar as default, MyCommandBar}