import * as React from 'react';
import {
  DocumentCard,
  DocumentCardActions,
  DocumentCardActivity,
  DocumentCardLocation,
  DocumentCardPreview,
  DocumentCardTitle,
  IDocumentCardPreviewProps,
  IDocumentCardStyles,
  DocumentCardType,
  DocumentCardImage,
  DocumentCardDetails,
  DocumentCardStatus,
  IDocumentCardDetailsStyles,
  IDocumentCardStatusStyles
} from 'office-ui-fabric-react/lib/DocumentCard';
import { ImageFit } from 'office-ui-fabric-react/lib/Image';
import { Text } from 'office-ui-fabric-react/lib/Text';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { TestImages } from '@uifabric/example-data';
import { Stack, IStackStyles, IStackTokens, IStackItemStyles, FontSizes,} from 'office-ui-fabric-react';
import {observer } from 'mobx-react';

//Styles
const _documentCardStyles: IDocumentCardStyles = {
  root: {
      margin: 10,
      padding:10,
      maxWidth: 3000,
      border: '1px solid lightgrey',
      width: 720,
      float: 'left'
  }
};

const docCardDetailsStyles: IDocumentCardDetailsStyles = {
    root: {
        marginLeft: 16,
        marginBottom: 10
    }
};

const docCardTitleStyles: IDocumentCardDetailsStyles = {
    root: {
        marginTop: 10,
        height: 32,
        fontSize: FontSizes.xxLarge
    }
};

const docCardStatusStyles: IDocumentCardStatusStyles = {
    root: {
        paddingLeft: 10,
        paddingTop: 10
    }
};

const stackTokens: IStackTokens = {
  childrenGap: 10,
  padding: 10
}

interface IDeviceDocumentCardProps{
    device: any;
}
@observer
class DocumentCardFonoDevice extends React.Component<IDeviceDocumentCardProps> {
  
  public render(): JSX.Element {
  
    return (
      <DocumentCard
        aria-label="Fono Mobile Device Details Document Card"
        onClick={this._onClick}
        styles={_documentCardStyles}
        type={DocumentCardType.normal}
      >
        <DocumentCardTitle styles={docCardTitleStyles}title="Device" />

        <DocumentCardDetails styles={docCardDetailsStyles}>
            <Label>Name</Label>
            <Text variant="medium">{this.props.device.DeviceName}</Text>

            <Stack horizontal horizontalAlign="start">
                <Stack.Item align="start" grow={true}>
                    <Label>Brand</Label>
                    <Text variant="medium">{this.props.device.Brand}</Text>
                    <Label>Status</Label>
                    <Text variant="medium">{this.props.device.status}</Text>
                    <Label>Price</Label>
                    <Text variant="medium">{this.props.device.price}</Text>
                </Stack.Item>
                {/* <Stack.Item align="stretch" grow={true}>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det4}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det5}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det6}</Text>
                </Stack.Item>
                <Stack.Item align="end" grow={true}>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det7}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det8}</Text>
                    <Label>Label</Label>
                    <Text variant="medium">{exampleData.det9}</Text>
                </Stack.Item> */}
            </Stack>
            
       {/*      <Stack horizontal horizontalAlign="start" tokens={stackTokens}>
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} iconProps={{ iconName: 'OneNoteLogo', styles: { root: { color: '#813a7c', fontSize: '80px', width: '80px', height: '80px' }}}} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.personaFemale} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.choiceGroupBarSelected} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.choiceGroupPieSelected} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.personaFemale} />
                <DocumentCardImage height={100} width={100} imageFit={ImageFit.contain} imageSrc={TestImages.personaMale} />

              </Stack>*/}
              </DocumentCardDetails>
        {/* <DocumentCardPreview {...previewProps} /> */}
        {/*<DocumentCardLocation
          location="Marketing Documents"
          locationHref="http://microsoft.com"
          ariaLabel="Location, Marketing Documents"
        />
        <DocumentCardTitle title="6 files were uploaded" />
        <DocumentCardActivity
          activity="Created Feb 23, 2016"
          people={[{ name: 'Annie Lindqvist', profileImageSrc: TestImages.personaFemale }]}
        />
        <DocumentCardActions
          actions={[
            {
              iconProps: { iconName: 'Share' },
              onClick: this._onActionClick.bind(this, 'share'),
              ariaLabel: 'share action'
            },
            {
              iconProps: { iconName: 'Pin' },
              onClick: this._onActionClick.bind(this, 'pin'),
              ariaLabel: 'pin action'
            },
            {
              iconProps: { iconName: 'Ringer' },
              onClick: this._onActionClick.bind(this, 'notifications'),
              ariaLabel: 'notifications action'
            }
          ]}
          views={432}
        />
        <DocumentCardStatus styles={docCardStatusStyles} status="Information">
            <Text variant="large">Groovy</Text>
        </DocumentCardStatus> */}
      </DocumentCard>
    );
  } 

  //Event handlers
  private _onActionClick(action: string, ev: React.SyntheticEvent<HTMLElement>): void {
    console.log(`You clicked the ${action} action`);
    ev.stopPropagation();
    ev.preventDefault();
  }

  private _onClick(): void {
    console.log('You clicked the card.');
  }
}

export { DocumentCardFonoDevice as default, DocumentCardFonoDevice };