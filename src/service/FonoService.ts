

interface IFonoService {
    getDevices(searchBrand: string, searchDevice: string, searchPosition: string): any;
    getLatest(searchBrand: string, searchLimit: string): any;
}

class FonoService {

    constructor(){
        
    }

    private baseUrl = 'https://fonoapi.freshpixl.com/v1/';
    private _fonoToken: string = '218f206afe4b49a1a387f5a66d519e07cf8c77cfa4ee2529';

    getDevices(searchBrand?: string, searchDevice?: string, searchPosition?: string): Promise<any> {
        return new Promise ((resolve, reject) => {
            try{

                !searchBrand ? searchBrand = "" : null;
                !searchDevice ? searchDevice = "" : null;
                !searchPosition ? searchPosition = "": null;

                const fonoDeviceUrl = new URL('getdevice', this.baseUrl);
                let params = {device: searchDevice, brand: searchBrand, position: searchPosition, token: this._fonoToken };

                Object.keys(params).forEach((key: Extract<keyof typeof params, string >) => {
                    fonoDeviceUrl.searchParams.append(key, params[key])
                });


                fetch(fonoDeviceUrl.href).then((response) => {
                    resolve(response.json());
                }).catch((error)=>{
                    reject({'status': 'error', 'message': `${error}`});
                }); 
                
            }catch(error){
                reject ({'status': 'error', 'message': `${error}`});
            }
        });
    }

    getLatest(searchBrand: string, searchLimit: string): any{
        const fonoLatestUrl = new URL('getlatest', this.baseUrl);
        let params = {brand: searchBrand, limit: searchLimit, token: this._fonoToken };

        Object.keys(params).forEach((key: Extract<keyof typeof params, string >) => {
            fonoLatestUrl.searchParams.append(key, params[key])
        });

    

        fetch(fonoLatestUrl.href)
        .then((response: { json: () => any; }) => {
                return response.json();
        })
        .then((myJson: any) => {
            console.log(myJson);
        });
        return;

    }

    
}

export {FonoService as default, FonoService, IFonoService};