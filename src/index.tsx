import * as React from "react";
import * as ReactDOM from "react-dom";
import { App } from "./components/App";
import { initializeIcons } from '@uifabric/icons';

initializeIcons();

ReactDOM.render(
    <App name="Fabric UI React Components Playground" />,
    document.getElementById("app")
);
