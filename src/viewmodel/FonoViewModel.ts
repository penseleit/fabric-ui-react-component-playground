import {FonoService, IFonoService} from '../service/FonoService';
import {RootModel, IRootModel} from '../models/RootModel';
import {observable, action, computed } from 'mobx';

interface IFonoViewModel{
    searchForDevices: () => void;
    clearSearchFields: () => void;
    deviceString: string;
    modelString: string;
    deviceCount: number;
    rootModel: IRootModel;
}

class FonoViewModel implements IFonoViewModel{
    
    private _fonoService: IFonoService;
    rootModel: IRootModel;

    constructor(){
        this._fonoService = new FonoService();
        this.rootModel = new RootModel();
    }
   
    searchForDevices(): void{
        try{
            this.rootModel.UIModel.isSearching = true;

            let jsonResponse: Promise<any> = this._fonoService.getDevices(
                this.rootModel.UIModel.modelSearchString, 
                this.rootModel.UIModel.deviceSearchString, 
                undefined
            ).then((response: any) => {
                if(response){
                    if(response.status){
                        if(response.status === 'error'){
                            throw(`${response.message}`);
                        }
                    }
                    if(Array.isArray(response)){
                        this.rootModel.DevicesModel.addDevices(response.map((item)=>{return item;}))
                    }
                }
            }).catch((error: any) => {
                console.error(`Error fetching devices: ${error}`);
                this.rootModel.DevicesModel.clearDevices();
            });         
        }catch(error){
            console.error(`Error: ${error}`);
            this.rootModel.DevicesModel.clearDevices();
        }
        finally{
            this.rootModel.UIModel.isSearching = false;
        }
    }

    @action
    updateDeviceSearchString(value: string): void{
        typeof value === "string" ? this.rootModel.UIModel.deviceSearchString = value : null;
    }

    @action
    updateModelSearchString(value: string): void{
        typeof value === "string" ? this.rootModel.UIModel.modelSearchString = value : null;
    }

    @action
    clearSearchFields(): void{
        this.rootModel.UIModel.deviceSearchString = "";
        this.rootModel.UIModel.modelSearchString = "";
    }

    @computed
    get deviceString(): string {
        return this.rootModel.UIModel.deviceSearchString;
    }

    @computed
    get modelString(): string{
        return this.rootModel.UIModel.modelSearchString;
    }

    @computed
    get deviceCount(): number{
        return this.rootModel.DevicesModel.devices.length; 
    }
}

export {FonoViewModel};