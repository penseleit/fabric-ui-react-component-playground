import {observable, action, computed } from 'mobx';

interface IUIModel{
    isSearching: boolean;
    deviceSearchString: string;
    modelSearchString: string;
}

class UIModel{

    constructor(){
        //Initialise model
    }

    @observable isSearching: boolean = false;
    @observable deviceSearchString: string = "";
    @observable modelSearchString: string = ""

 
}

export {UIModel as default, UIModel, IUIModel};