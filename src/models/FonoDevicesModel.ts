import{ observable, action, computed } from 'mobx';

interface IFonoDevicesModel {
    devices: Object[];
    addDevices(newDevices: Object[]): void;
    clearDevices(): void;
}

class FonoDevicesModel {

    constructor(){
        //Initialise data
    }

    @observable devices: Object[] = [];


    @action
    addDevices(newDevices: Object[]): void{
        if(newDevices && newDevices.length > 0){
            this.devices.length = 0;
            this.devices = [...newDevices];
        }
        console.info(`No. of devices returned: ${this.devices.length}`);
    }

    @action
    clearDevices(): void{
        this.devices.length = 0;
        this.devices = [];
    }
  
}

export {FonoDevicesModel as default, FonoDevicesModel, IFonoDevicesModel};