import {observable, action, computed} from 'mobx';
import { FonoDevicesModel, IFonoDevicesModel } from './FonoDevicesModel';
import { UIModel, IUIModel } from './UIModel';


interface IRootModel{
    DevicesModel: IFonoDevicesModel;
    UIModel: IUIModel;
}

class RootModel{

    constructor(){
        this.DevicesModel = new FonoDevicesModel();
        this.UIModel = new UIModel();
    }

    @observable DevicesModel: IFonoDevicesModel;
    @observable UIModel: IUIModel;

}

export {RootModel as default, RootModel, IRootModel};