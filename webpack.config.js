let path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
 

module.exports = {
    mode: "development",
    entry: "./src/index.tsx",

    output: {
        filename: 'index_bundle.js',
        path: path.resolve(__dirname, 'dist'),
      },


    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: "ts-loader"
                    }
                ]
            },
            {   test: /\.css$/, 
                use: [
                'style-loader',
                'css-loader' 
                ]
            },
            {
                test: /\.(png|jpe?g|gif)$/i,
                loader: 'file-loader',
                options: {
                  outputPath: 'images',
                }
            },
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            }
        ]
    },
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js"]
    },
    // Enable sourcemaps for debugging webpack's output.
    devtool: "source-map",
    devServer: {
        contentBase: path.join(__dirname,'public'),
        onListening: function(server) {
            const port = server.listeningApp.address().port;
            console.log('Listening on port:', port);
          },
          open: true
    },
    watchOptions: {
        ignored: /node_modules/
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'Fono API Searcher',
            template: 'src/index.html'
        })
      ]

};
